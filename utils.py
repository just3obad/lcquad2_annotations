"""
    A helper script with helper functions for reading writing files.
"""

import json
import pickle
import csv
import textwrap

# Load Files


def load_json(path):
    with open(path) as data_file:
        return json.load(data_file)


def load_txt(name):
    with open(name) as data_file:
        data = data_file.readlines()
    data = [d.replace("\n", "") for d in data]
    return data


def load_pickle(path):
    with open(path) as data_file:
        return pickle.load(data_file)


def load_csv(path):
    with open(path) as data_file:
        return [row for row in csv.reader(data_file, delimiter=",")]


# Save Files


def save_json(data, name):
    with open(name, "w") as data_file:
        json.dump(data, data_file, sort_keys=False, indent=4, separators=(',', ': '))

def save_text(data, name):
    with open(name, "w") as data_file:
        for line in data:
            data_file.write(line+"\n")


def write_to_text_file(file, lines):
    with open(file, "a+") as data_file:
        data_file.writelines(lines)

