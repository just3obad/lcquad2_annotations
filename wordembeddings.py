import torch
from flair.data import Sentence
from flair.embeddings import FlairEmbeddings, StackedEmbeddings, WordEmbeddings, DocumentPoolEmbeddings
from tqdm import tqdm
import pickle
from utils import *
from joblib import Parallel, delayed
# from annotations import generate_all_substrings, clean_string, get_entities_predicates_nnqt


class DocSimilarity:

    def __init__(self):
        # embeddings = FlairEmbeddings('news-forward')
        embeddings = WordEmbeddings('glove')
        self.document_embeddings = DocumentPoolEmbeddings([embeddings])
        self.embeddings_matrix = []

    def check_similarity_no_cache(self, doc_1, doc_2):
        try:
            cos = torch.nn.CosineSimilarity(dim=0, eps=1e-6)
            doc_1_sent = Sentence(doc_1)
            doc_2_sent = Sentence(doc_2)
            self.document_embeddings.embed(doc_1_sent)
            self.document_embeddings.embed(doc_2_sent)
            return cos(doc_1_sent.get_embedding(), doc_2_sent.get_embedding()).item()
        except:
            return 0

    def check_similarity(self, doc_1, doc_2):
        if self.embeddings_matrix and doc_1 in self.embeddings_matrix and doc_2 in self.embeddings_matrix:
            cos = torch.nn.CosineSimilarity(dim=0, eps=1e-6)
            return cos(self.embeddings_matrix[doc_1], self.embeddings_matrix[doc_2]).item()
        else:
            return None

    def load_embeddings(self, path):
        print("Loading Embeddings")
        self.embeddings_matrix = pickle.load(open(path, "rb"))


    def cache_embeddings(self):
        print("Caching Embeddings")
        corpus = list(load_json("data/data_sentences_vector.json").keys())[:10]

        results = Parallel(n_jobs=-1, backend="threading", verbose=-1)(map(delayed(self.get_embedding_vector), corpus))
        embeddings_cache = {item[0]: item[1] for item in results if item}

        filehandler = open("data/embeddings_matrix.obj", 'wb')
        pickle.dump(embeddings_cache, filehandler)
        # save_json(embeddings_cache, "data/embeddings_sentences.json")

    def get_embedding_vector(self, doc):
        if doc:
            doc_sentence = Sentence(doc)
            self.document_embeddings.embed([doc_sentence])
            return [doc,doc_sentence.embedding]
        else:
            return None


# def prepare_caching_vectors():
#     data = load_json("dataset/lcquad2.0_clean.json")
#
#     vectors_dict = {}
#     for row in tqdm(data):
#         clean_entities = []
#         entities_nnqt = get_entities_predicates_nnqt(row["NNQT_question"])
#
#         substrings =   generate_all_substrings(row["question"])
#
#         if row["paraphrased_question"]:
#             substrings = substrings + generate_all_substrings(row["paraphrased_question"])
#
#         # print(entities_nnqt)
#         # print(substrings)
#
#         for i in substrings:
#             vectors_dict[clean_string(i)] = ""
#
#         # print(vectors_dict)
#         # break
#
#     print("Saved %s sentences" % len(vectors_dict))
#     save_json(vectors_dict, "data/data_sentences_vector.json")






if __name__ == '__main__':
    print("MAIN")
    doc_sim = DocSimilarity()
    # doc_sim.load_embeddings("data/embeddings_matrix.obj")
    # doc_sim.cache_embeddings()
    # print(len(doc_sim.get_embedding_vector("arbeitgeber vereinbart benzinkosten bezahle firmenwagen privat nutze reduziert versteuernden geldwerten vorteil one regel bestimme")[0]))


    doc_1 = "occupation"
    doc_2 = "practice law"
    # corpus = [doc_1, doc_2]

    # print(doc_sim.get_embedding_vector(doc_1))
    #
    print(doc_sim.check_similarity_no_cache(doc_1, doc_2))

    # prepare_caching_vectors()







