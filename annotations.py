# -*- coding: utf-8 -*-
from utils import *
import re
from fuzzywuzzy import fuzz
import string
from tqdm import tqdm
from spacy.lang.en import English
nlp = English()
import sys
# from spacy.lang.en.stop_words import STOP_WORDS
from wordembeddings import DocSimilarity

# LABELS = load_json("data/lcquad_entities_wikidata.json")

def get_entities_predicates_nnqt(row):
    """
        Takes an NNQT returns the mentions. Entities and predicates
    """
    return re.findall(r"{(.*?)}", row, flags=re.IGNORECASE)


def generate_all_substrings(string):
    """
        Generates all the possible substrings from a given question
    :param string: question
    :return: substrings list
    """
    string_list = string.split(" ")
    # print string_list
    result = set()
    for index in range(len(string_list)):
        for curr in range(len(string_list)+1):
            substring = " ".join(string_list[index: index+curr])
            if substring:
                result.add(substring)
    result = list(result)
    result.sort(key=len, reverse=True)
    return result


def get_best_match(pattern, string_list):
    """
        Takes a pattern and a list of strings.
    :param pattern: string
    :param string_list: list of strings
    :return: The string that best matches the pattern
    """
    best_score = 0
    # best_index = 0

    scores = []

    for index, string in enumerate(string_list):
        score = fuzz.token_set_ratio(pattern.encode("ascii", "ignore"), string.encode("ascii", "ignore"))
        scores.append([string, score])
        if score >= best_score:
            best_score = score
        #     best_index = index

    # print best_score
    filtered_scores = []

    for i, s in enumerate(scores):
        if s[1] == best_score:
            filtered_scores.append(s[0])

    # try:
    best_match = min(filtered_scores, key=lambda x: abs(len(x) - len(pattern)))
    # except:
    #     print pattern, filtered_scores
    #     sys.exit(0)

    return best_match, best_score


def clean_string(input_string):
    """
        Takes a string and remove punctuations.
    :param input_string:
    :return:
    """
    input_string = input_string.strip()
    input_string = input_string.lower()
    input_string = remove_punc(input_string)
    # input_string = input_string.translate(None, string.punctuation)
    # input_string_tokens = nlp(u"%s" % input_string)
    # input_string = " ".join([i.text for i in input_string_tokens if i.is_stop is False])
    return input_string


def prepare_mentions_nnqt(nnqt_question, question):
    entity_list = [{"label": ent} for ent in get_entities_predicates_nnqt(nnqt_question)]
    # print entities
    # question = clean_string(question)
    question_substrings = generate_all_substrings(question)

    for row in entity_list:
        if not row["label"]:
            row["span"] = None
            row["score"] = None
            row["valid"] = False
            row["method"] = None
            continue

        match, score = get_best_match(row["label"], question_substrings)
        row["span"] = match
        row["score"] = score
        row["method"] = "lev distance"
        if score >= 75:
            row["valid"] = True
        else:
            row["valid"] = False
    return entity_list


def check_invalid_entity_mentions():
    data = load_json("data/lcquad2.0_mentions.json")
    dirty = []
    clean = []
    for row in data:
        mentions = row["entity_mentions"]
        flag = True
        for i in mentions:
            if "valid" in i and i["valid"] is False:
                flag = False
                break

        if flag:
            clean.append(row)
        else:
            dirty.append(row)

    print("No. invalid rows", len(dirty))
    save_json(dirty, "data/lcquad2.0_mentions_not_valid.json")

    print("No. valid rows", len(clean))
    save_json(clean, "data/lcquad2.0_mentions_clean.json")


def remove_punc(text):
    # translator = str.maketrans(string.punctuation, ' ' * len(string.punctuation))  # map punctuation to space
    # text = text.translate(translator)
    return re.sub(r"„|%|–|·|-|:|\+|=|,|§|\"|\.|/|“|;|&|\)|\(|\?|´|,|_", " ", text)


def main():
    """
        Main method for running the code.
    :return:
    """
    data = load_json("dataset/lcquad2.0_clean.json")
    for row in tqdm(data):
        # sparql = row["sparql_wikidata"]
        nnqt = row["NNQT_question"]
        question = row["question"]
        question_paraphrased = row["paraphrased_question"]
        # row["entity_mentions"] = prepare_mentions_sparql(sparql, question)
        row["entity_mentions_question"] = prepare_mentions_nnqt(nnqt, question)
        try:
            row["entity_mentions_question_paraphrased"] = prepare_mentions_nnqt(nnqt, question_paraphrased)
        except:
            row["entity_mentions_question_paraphrased"] = []
    save_json(data, "data/lcquad2.0_mentions_2.json")



def fix_mentions_we():
    doc_sim = DocSimilarity()

    data = load_json("data/lcquad2.0_mentions_2.json")
    for row in tqdm(data):
        for span in row["entity_mentions_question"]:
            if not span["valid"]:
                label = span["label"]
                question = row["question"]
                strings = generate_all_substrings(question)
                strings_scores = [[s, doc_sim.check_similarity_no_cache(label, s)] for s in strings if s]
                strings_scores_sorted = sorted(strings_scores, key= lambda x:x[1], reverse=True)
                span["span"] = strings_scores_sorted[0][0]
                span["score"] = strings_scores_sorted[0][1]
                span["method"] = "we"
        for span in row["entity_mentions_question_paraphrased"]:
            if not span["valid"]:
                label = span["label"]
                question = row["paraphrased_question"]
                strings = generate_all_substrings(question)
                strings_scores = [[s, doc_sim.check_similarity_no_cache(label, s)] for s in strings if s]
                strings_scores_sorted = sorted(strings_scores, key= lambda x:x[1], reverse=True)
                span["span"] = strings_scores_sorted[0][0]
                span["score"] = strings_scores_sorted[0][1]
                span["method"] = "we"

    save_json(data, "data/lcquad2.0_mentions_2_updated.json")


def add_mention_type():
    data = load_json("data/lcquad2.0_mentions_2_updated.json")

    temp_types = {
        'seven': ["E", "P", "E"],
        'three': ["E", "P", "E", "E"],
        'six': ["P", "E"],
        'sixteen': ["P", "E"],
        'fifteen': ["E", "P"],
        'eleven': ["E", "P", "P", "E"],
        'twenty-one': ["E", "P", "P", "E"],
        'four': ["P", "E", "P", "D"],
        'two': ["P", "E", "P", "E"],
        'five': ["P", "P", "E"],
        'fourteen': ["P", "P", "E", "P", "E"],
        'twenty': ["P", "E", "P"],
        'zero': ["P", "E", "P", "E"],
        'twelve': ["P", "P", "E"],
        'eight': ["P", "P", "E"],
        'ten': ["P", "E", "O", "L"],
        'seventeen': ["E", "P", "E"],
        'nine': ["E", "P", "E"],
        'thirteen': ["E", "L"],
        'eighteen': ["E", "L"],
        'nineteen': ["P", "E"],
        'one': ["P", "E"]}

    for row in data:
        tmp_id = row["template_label"]
        for span, type_ in zip(row["entity_mentions_question"], temp_types[tmp_id]):
            span["type"] = type_
        for span, type_ in zip(row["entity_mentions_question_paraphrased"], temp_types[tmp_id]):
            span["type"] = type_

    save_json(data, "data/lcquad2.0_mentions_2_updated_types.json")


def check_tmp():
    data = load_json("data/lcquad2.0_mentions_2_updated.json")
    for row in data:
        if row["template_label"] == "twelve":
            print(row["template_label"])
            print(row["NNQT_question"])
            print(row["subgraph"])
            print(row["template"])
            print(row["sparql_wikidata"])
            # print(row["entity_mentions_question"])
            break


if __name__ == '__main__':
    print("MAIN")
    # main()
    # print generate_all_substrings("i am bewolf")
    # fix_mentions_we()
    add_mention_type()

    # check_tmp()










